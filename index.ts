import ToDoUseCasesFactory from "./toDo/UseCases/factory";
import config from "./config";

const useCases = {
  add_item_use_case: ToDoUseCasesFactory.addItemUseCase({ config }),
  get_all_items_use_case: ToDoUseCasesFactory.getAllItemsUseCase({ config }),
  remove_item_by_id_use_case: ToDoUseCasesFactory.removeItemByIdUseCase({
    config
  })
};

export default useCases;
