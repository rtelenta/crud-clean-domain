import domain from "../";

beforeEach(() => {
  localStorage.clear();
});

describe("toDo: Remove Item", () => {
  it("remove Item by id success", () => {
    domain.add_item_use_case.execute({
      text: "test"
    });
    const items = domain.get_all_items_use_case.execute();
    const callback = domain.remove_item_by_id_use_case.execute({
      id: items[0].id
    });
    expect(callback).toBe(true);
  });

  it("remove Item by id fail", () => {
    domain.add_item_use_case.execute({
      text: "test"
    });

    const callback = domain.remove_item_by_id_use_case.execute({
      id: "123"
    });
    expect(callback).toBe(false);
  });
});
