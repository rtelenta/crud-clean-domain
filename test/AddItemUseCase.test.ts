import domain from "../";

beforeEach(() => {
  localStorage.clear();
});

describe("toDo: Add Item is ok", () => {
  it("add new item", () => {
    const callback = domain.add_item_use_case.execute({
      text: "test"
    });
    expect(callback).toBe(true);
  });

  it("add new item with empty text", () => {
    const callback = domain.add_item_use_case.execute({
      text: ""
    });
    expect(callback).toBe(false);
  });
});
