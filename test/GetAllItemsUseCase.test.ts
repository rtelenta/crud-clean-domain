import domain from "../";

beforeEach(() => {
  localStorage.clear();
});

describe("toDo: GetItems", () => {
  it("get all items with one result", () => {
    domain.add_item_use_case.execute({
      text: "test"
    });

    const callback = domain.get_all_items_use_case.execute();
    expect(callback.length).toBe(1);
  });
});

describe("toDo: GetItems", () => {
  it("get all items with zero results", () => {
    const callback = domain.get_all_items_use_case.execute();
    expect(callback.length).toBe(0);
  });
});
