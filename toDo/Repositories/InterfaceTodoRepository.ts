import TodoRepositoryPersistInput from "../Inputs/TodoRepositoryPersistInput";
import AddItemInput from "../Inputs/AddItemInput";
import SelectItemByInput from "../Inputs/SelectItemByIdInput";

export default interface InterfaceTodoRepository {
  removeById(params: SelectItemByInput): boolean;
  all(): Array<AddItemInput>;
  persist(params: TodoRepositoryPersistInput): boolean;
}
