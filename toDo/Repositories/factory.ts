import LocalStorageTodoRepository from "./LocalStorageTodoRepository";
import TodoEntiesFactory from "../Entities/factory";
import ConfigInput from "../Inputs/ConfigInput";

interface IParams {
  config: ConfigInput;
}

export default class TodoRepositoriesFactory {
  static localStorageTodoRepository = ({ config }: IParams) =>
    new LocalStorageTodoRepository({
      config,
      itemEntityFactory: TodoEntiesFactory.itemEntity
    });
}
