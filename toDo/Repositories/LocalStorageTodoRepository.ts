import { ItemEntityInput } from "./../Inputs/ItemEntityInput";
import InterfaceTodoRepository from "./InterfaceTodoRepository";
import ConfigInput from "../Inputs/ConfigInput";
import TodoRepositoryPersistInput from "../Inputs/TodoRepositoryPersistInput";
import AddItemInput from "../Inputs/AddItemInput";
import SelectItemByInput from "../Inputs/SelectItemByIdInput";

interface IParams {
  config: ConfigInput;
  itemEntityFactory: ItemEntityInput;
}

export default class TodoLocalStorageRepository
  implements InterfaceTodoRepository {
  private localStorage: any;
  private keyStorage: string;
  private _config: ConfigInput;
  private _itemEntityFactory: ItemEntityInput;

  constructor({ config, itemEntityFactory }: IParams) {
    this.localStorage = window.localStorage;
    this.keyStorage = "listItems";
    this._config = config;
    this._itemEntityFactory = itemEntityFactory;
  }

  private existKeyInStorage(): boolean {
    return !!this.localStorage.getItem(this.keyStorage);
  }

  public all() {
    const getStorageData = JSON.parse(
      this.localStorage.getItem(this.keyStorage)
    );
    const itemsInStorage = this.existKeyInStorage() ? getStorageData : [];

    return itemsInStorage.map(
      (item: AddItemInput) => this._itemEntityFactory(item).data
    );
  }

  public removeById({ id }: SelectItemByInput) {
    const listItems = this.all();
    const filteredList = listItems.filter(
      (item: AddItemInput) => item.id !== id
    );

    if (filteredList.length === listItems.length) {
      return false;
    } else {
      this.localStorage.setItem(this.keyStorage, JSON.stringify(filteredList));
      return true;
    }
  }

  public persist({ itemEntity }: TodoRepositoryPersistInput) {
    const listItems = this.all();
    listItems.push(itemEntity.data);
    this.localStorage.setItem(this.keyStorage, JSON.stringify(listItems));
    return true;
  }
}
