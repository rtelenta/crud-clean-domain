import AddItemUseCase from "./AddItemUseCase";
import TodoRepositoriesFactory from "../Repositories/factory";
import TodoEntitiesFactory from "../Entities/factory";
import GetAllItemsUseCase from "./GetAllItemsUseCase";
import ConfigInput from "../Inputs/ConfigInput";
import RemoveItemByIdUseCase from "./RemoveItemByIdUseCase";

interface IParams {
  config: ConfigInput;
}

export default class ToDoUseCasesFactory {
  static addItemUseCase = ({ config }: IParams) =>
    new AddItemUseCase({
      repository: TodoRepositoriesFactory.localStorageTodoRepository({
        config
      }),
      itemEntityFactory: TodoEntitiesFactory.itemEntity
    });

  static getAllItemsUseCase = ({ config }: IParams) =>
    new GetAllItemsUseCase({
      repository: TodoRepositoriesFactory.localStorageTodoRepository({
        config
      })
    });

  static removeItemByIdUseCase = ({ config }: IParams) =>
    new RemoveItemByIdUseCase({
      repository: TodoRepositoriesFactory.localStorageTodoRepository({
        config
      })
    });
}
