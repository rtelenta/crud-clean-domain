import InterfaceTodoRepository from "../Repositories/InterfaceTodoRepository";

interface IParams {
  repository: InterfaceTodoRepository;
}

export default class GetAllItemsUseCase {
  private _repository: InterfaceTodoRepository;

  constructor({ repository }: IParams) {
    this._repository = repository;
  }

  execute() {
    return this._repository.all();
  }
}
