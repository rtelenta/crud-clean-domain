import InterfaceTodoRepository from "../Repositories/InterfaceTodoRepository";
import SelectItemByInput from "../Inputs/SelectItemByIdInput";

interface IParams {
  repository: InterfaceTodoRepository;
}

export default class RemoveItemByIdUseCase {
  private _repository: InterfaceTodoRepository;

  constructor({ repository }: IParams) {
    this._repository = repository;
  }

  execute({ id }: SelectItemByInput) {
    return this._repository.removeById({ id });
  }
}
