import { ItemEntityInput } from "./../Inputs/ItemEntityInput";
import InterfaceTodoRepository from "../Repositories/InterfaceTodoRepository";

interface IParams {
  repository: InterfaceTodoRepository;
  itemEntityFactory: ItemEntityInput;
}

interface IExecuteParams {
  text: string;
}

export default class AddItemUseCase {
  private _repository: InterfaceTodoRepository;
  private _itemEntityFactory: ItemEntityInput;

  constructor({ repository, itemEntityFactory }: IParams) {
    this._repository = repository;
    this._itemEntityFactory = itemEntityFactory;
  }

  private generateId() {
    return (
      "_" +
      Math.random()
        .toString(36)
        .substr(2, 9)
    );
  }
  execute(data: IExecuteParams) {
    const itemEntity = this._itemEntityFactory({
      ...data,
      id: this.generateId(),
      completed: false
    });

    if (!itemEntity.isValidText()) {
      return false;
    }

    return this._repository.persist({ itemEntity });
  }
}
