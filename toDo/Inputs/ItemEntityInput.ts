import AddItemInput from "./AddItemInput";
import ItemEntity from "../Entities/ItemEntity";

export type ItemEntityInput = (param: AddItemInput) => ItemEntity;
