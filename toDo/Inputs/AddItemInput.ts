export default interface AddItemInput {
  id: string;
  text: string;
  completed: boolean;
}
