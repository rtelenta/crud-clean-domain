import ItemEntity from "../Entities/ItemEntity";

export default interface TodoRepositoryPersistInput {
  itemEntity: ItemEntity;
}
