export default class ItemEntity {
  private _id: string;
  private _text: string;
  private _completed: boolean;

  constructor(id: string, text: string, completed: boolean) {
    this._id = id;
    this._text = text;
    this._completed = completed;
  }

  public isValidText() {
    return this._text !== "";
  }

  get data() {
    return { id: this._id, text: this._text, completed: this._completed };
  }
}
