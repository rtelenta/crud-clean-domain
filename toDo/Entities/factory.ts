import ItemEntity from "./ItemEntity";
import AddItemInput from "../Inputs/AddItemInput";

export default class TodoEntitiesFactory {
  static itemEntity = (data: AddItemInput) =>
    new ItemEntity(data.id, data.text, data.completed);
}
